#!/bin/bash

if [ sudo apt update && sudo apt -y upgrade ]; then
else
    echo "apt update/upgrade failed"
    exit 1
fi;

read -r -p "Install tools? [Y/n]" response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]] | [ -z $response ]; then
    sudo apt -y install curl zsh git python-dev python3-dev cmake build-essential pkg-config vim
fi

read -r -p "Install redshift? [Y/n]" response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]] | [ -z $response ]; then
    sudo apt -y install redshift redshift-gtk
fi

sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

read -r -p "Install Sublime? [Y/n]" response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]] | [ -z $response ]; then
    curl -L git.io/sublimetext | sh
fi

read -r -p "Install fonts? [Y/n]" response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]] | [ -z $response ]; then
    git clone https://github.com/powerline/fonts.git /tmp/fonts
    /tmp/fonts/install.sh
fi

read -r -p "Setup dotfiles? [Y/n]" response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]] | [ -z $response ]; then
    if [ -x setup.sh ]; then
        sh setup.sh
    else
        sh -c "$(curl -ksSL http://bit.do/4fex-setup)"
    fi
fi
