#!/bin/bash

git clone --bare https://4fex@bitbucket.org/4fex/dotconf.git $HOME/.cfg
echo ".cfg" > $HOME/.gitignore
alias dotconf='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
mkdir -p .dotconf-backup
dotconf checkout
if [ $?=0 ]; then
    echo "Checked out config.";
else
    echo "Backing up pre-existing dot files.";
    dotconf checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} sh -c 'mkdir -p $(dirname {}); mv {} .dotconf-backup/{}'
fi;
dotconf checkout
dotconf config status.showUntrackedFiles no

vim +PluginInstall +qall
python $HOME/.vim/bundle/YouCompleteMe/install.py
